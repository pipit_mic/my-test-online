/*
File	: polymorphism.h
Poject	: polymorphism
===============================================================
<< History Revision >>
Date			Author		Version		Contents
2020-04-09		Pipit L.	1.00		First released
===============================================================
*/

#ifndef POLYMORPHISM_H
#define POLYMORPHISM_H

#define _PI	3.1415926535897

class Shape2D
{
	public:
		virtual double getArea(void) = 0;
		double calCostOfLand(Shape2D *pShape2D)
		{
			return pShape2D->getArea() * 100.0;
		}
};

class Rectangle :public Shape2D
{
	private:
		double width;
		double height;
	public:
		Rectangle(double _width_, double _height_)
		{
			width = _width_;
			height = _height_;
		}
		double getArea(void)
		{
			return width * height;
		}
};

class Circle :public Shape2D
{
	private:
		double radius;
	public:
		Circle(double _radius_)
		{
			radius = _radius_;
		}
		double getArea(void)
		{
			return _PI * radius * radius;
		}
};

class Triangle :public Shape2D
{
	private:
		double base;
		double height;
	public:
		Triangle(double _base_, double _height_)
		{
			base = _base_;
			height = _height_;
		}
		double getArea()
		{
			return 0.5 * base * height;
		}
};

#endif // !POLYMORPHISM_H
