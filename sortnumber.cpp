#include <iostream>

using namespace std;

void main(void)
{
	int i=0, j=0, value=0;
	int Data[7] = { 5,3,4,1,7,6,8 };
	int ArraySize = 7;

	printf("Print data before sort");
	printf("\n \n \n");

	for (int i = 0; i < ArraySize; i++)
	{
		printf("%d ", Data[i]);
	}
	printf("\n \n \n");

	for (i = 0; i < ArraySize; i++)
	{
		value = Data[i];

		for (j = i - 1; j >= 0; j--)
		{
			if (Data[j] <= value) break;
			{
				Data[j + 1] = Data[j];
			}
		}

		Data[j + 1] = value;
	}

	printf("Print data after sort");
	printf("\n \n \n");

	for (i = 0; i < ArraySize; i++)
	{
		printf("%d ", Data[i]);
	}
	printf("\n \n \n");

	system("pause");
}