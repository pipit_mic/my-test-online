#include <iostream>
#include <vector>
#include "test_vector.h"

using namespace std;

void main(void)
{
	vector<int> vector1;
	vector1.push_back(10);
	vector1.push_back(9);
	vector1.push_back(8);
	vector1.push_back(7);
	vector1.push_back(6);
	cout << "vector1.front() : " << vector1.front() << endl;	// 10
	cout << "vector1.back()  : " << vector1.back()  << endl;	// 6
	cout << "vector1.size()  : " << vector1.size()  << endl;	// 5
	vector1.pop_back();
	cout << "vector1.front() : " << vector1.front() << endl;	// 10
	cout << "vector1.back()  : " << vector1.back()  << endl;	// 7
	cout << "vector1.size()  : " << vector1.size()  << endl;	// 4
	vector1.pop_back();
	vector1.pop_back();
	cout << "vector1.front() : " << vector1.front() << endl;	// 10
	cout << "vector1.back()  : " << vector1.back()  << endl;	// 9
	cout << "vector1.size()  : " << vector1.size()  << endl;	// 2
	cout << "vector1.empty() : " << vector1.empty() << endl;	// empty flag:0 is a non-empty vector,1 is a empty vector 
	cout << "=========================================" << endl;

	vector<int> vector2{ 4,6,1,7 };
	cout << "vector2.size() : " << vector2.size() << endl;	// 4
	cout << "=========================================" << endl;

	vector<int> vector3(9);
	cout << "vector3.size() : " << vector3.size() << endl;	// 9
	cout << "=========================================" << endl;

	// Copy vector to vector
	vector<int> vector4(100, 0);
	vector<int> vector5(5, 0);
	vector5 = vector4;
	cout << "vector4.size() : " << vector4.size() << endl;	// 100
	cout << "vector5.size() : " << vector5.size() << endl;	// 100
	cout << "=========================================" << endl;

	// Copy vector to vector
	vector<int> vector6(5, 0);
	vector<int> vector7(100, 0);
	vector7 = vector6;
	cout << "vector6.size() : " << vector6.size() << endl;	// 5
	cout << "vector7.size() : " << vector7.size() << endl;	// 5
	cout << "=========================================" << endl;

	// Initialization all of variables of vector
	vector<int> vector8(5, -9);		// same memset() in C
	cout << "vector8[0] : " << vector8[0] << endl;	// -9
	cout << "vector8[1] : " << vector8[1] << endl;	// -9
	cout << "vector8[2] : " << vector8[2] << endl;	// -9
	cout << "vector8[3] : " << vector8[3] << endl;	// -9
	cout << "vector8[4] : " << vector8[4] << endl;	// -9
	cout << "=========================================" << endl;

	// Insert data into a vector
	vector<int> vector9{ 1,2,3,4,5 };
	cout << "vector9[0]     : " << vector9[0]     << endl;	// 1
	cout << "vector9[1]     : " << vector9[1]     << endl;	// 2
	cout << "vector9[2]     : " << vector9[2]     << endl;	// 3
	cout << "vector9[3]     : " << vector9[3]     << endl;	// 4
	cout << "vector9[4]     : " << vector9[4]     << endl;	// 5
	cout << "vector9.size() : " << vector9.size() << endl;	// 5
	vector9.insert(vector9.begin() + 3, -8);				// Insert -8 into the 3rd position of vector9
	cout << "Insert data vector..." << endl;
	cout << "vector9.size() : " << vector9.size() << endl;	// 6
	cout << "vector9[0]     : " << vector9[0] << endl;		// 1
	cout << "vector9[1]     : " << vector9[1] << endl;		// 2
	cout << "vector9[2]     : " << vector9[2] << endl;		// 3
	cout << "vector9[3]     : " << vector9[3] << endl;		// -8
	cout << "vector9[4]     : " << vector9[4] << endl;		// 4
	cout << "vector9[5]     : " << vector9[5] << endl;		// 5
	cout << "=========================================" << endl;

	// Insert vector to vector
	vector<int> vector10{ 0,1,2,3 };
	vector<int> vector11{ 4,5,6,7 };
	// Insert the vector10 into the vector11 that start insert at 2nd position of vector11
	vector11.insert(vector11.begin()+2, vector10.begin() , vector10.end());

	cout << "vector11.size() : " << vector11.size() << endl;	// 8
	cout << "vector11[0]     : " << vector11[0] << endl;		// 4
	cout << "vector11[1]     : " << vector11[1] << endl;		// 5
	cout << "vector11[2]     : " << vector11[2] << endl;		// 0
	cout << "vector11[3]     : " << vector11[3] << endl;		// 1
	cout << "vector11[4]     : " << vector11[4] << endl;		// 2
	cout << "vector11[5]     : " << vector11[5] << endl;		// 3
	cout << "vector11[6]     : " << vector11[6] << endl;		// 6
	cout << "vector11[7]     : " << vector11[7] << endl;		// 7
	cout << "=========================================" << endl;
	
	// Vector interator (pointer of vector)
	vector <int> vector12{ 6,56,0,98,5 };
	vector <int>::iterator iteratorVector12 = vector12.begin();

	cout << "vector12[0] : " << vector12[0] << endl;	// 6
	cout << "vector12[1] : " << vector12[1] << endl;	// 56
	cout << "vector12[2] : " << vector12[2] << endl;	// 0
	cout << "vector12[3] : " << vector12[3] << endl;	// 98
	cout << "iteratorVector12 + 0   : " << *(iteratorVector12 + 0) << endl;	// 6
	cout << "iteratorVector12 + 1   : " << *(iteratorVector12 + 1) << endl;	// 56
	cout << "iteratorVector12 + 2   : " << *(iteratorVector12 + 2) << endl;	// 0
	cout << "iteratorVector12 + 3   : " << *(iteratorVector12 + 3) << endl;	// 98
	cout << "=========================================" << endl;

	// Loopping vector by using a vector interator
	vector <int> vector13{ 4,50,85,999 };
	vector <int>::iterator iteratorVector13;

	for (iteratorVector13 = vector13.begin(); iteratorVector13 != vector13.end(); iteratorVector13++)
	{
		cout << ' ' << *iteratorVector13 << '\n';
	}
	// 4
	// 50
	// 85
	// 999
	cout << "=========================================" << endl;

	vector<int> vector14{ "fewf","efwfwf" };
	cout << "vector14.size() : " << vector14.size() << endl;	// 
}