#include <iostream>
#include <vector>
#include <list>
// 456
using namespace std;

// Declare Node
struct Node
{
	int num;
	Node *next;
};

// Declare starting (Head) node
struct Node *head = NULL;

// Insert node at start
void insertNode(int n)
{
	struct Node *newNode = new Node;
	newNode->num = n;
	newNode->next = head;
	head = newNode;
}


// delete node from start
void deleteItem(void)
{
	if (head == NULL)
	{
		cout << "List is empty!" << endl;
		return;
	}
	cout << head->num << " is removed." << endl;
	head = head->next;
}

class Point
{
private:
	static int x, y;
	static int count;
public:
	Point(int x = 0, int y = 0);
	void print();
	static int getCount();
};
int Point::x = 0;
int Point::y = 0;
int Point::count = 0;
int Point::getCount()
{
	x = 0;
	return count;
}

void main(void)
{
//	int a = 4;
//	float b = 5.554;
//	b = a;
//	cout << "b = a : " << b << endl;

	insertNode(121);
	insertNode(122);
	insertNode(123);
	cout << "Add 121 in Node-1st" << endl;
	cout << "Add 122 in Node-2nd" << endl;
	cout << "Add 123 in Node-3rd" << endl;
	cout << "==============================================" << endl;
	
	struct Node *numShow = head;	// Must be declared this structure afer added the data of node complet. 
	
	cout << "Data in Node-3rd is " << numShow->num << endl;		numShow = numShow->next;	// Show and update for next node.
	cout << "Data in Node-2nd is " << numShow->num << endl;		numShow = numShow->next;	// Show and update for next node.
	cout << "Data in Node-1st is " << numShow->num << endl;		numShow = numShow->next;	// Show and update for next node.
	cout << "==============================================" << endl;

	//deleteItem();
	//deleteItem();
	//deleteItem();
	//head = head->next;
	// Check a empty node
	//if (head == NULL)
	//{
		//cout << "List is empty!" << endl;
	//}





}