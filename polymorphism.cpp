/*
File	: polymorphism.cpp
Poject	: polymorphism
===============================================================
<< History Revision >>
Date			Author		Version		Contents
2020-04-09		Pipit L.	1.00		First released
===============================================================
*/

#include <iostream>
#include "polymorphism.h"

using namespace std;

void main(void)
{
	// Declare the pointer for upcasting the sub-class to super-class
	Shape2D *pShape2D_Rectangle = new Rectangle(56.98, 9.423);
	Shape2D *pShape2D_Circle    = new Circle(7.5891);
	Shape2D *pShape2D_Triangle  = new Triangle(658.394, 80.2255);

	// Get the area and calculate the cost of land for other object 2D
	double areaRectangle       = pShape2D_Rectangle->getArea();
	double costOfLandRectangle = pShape2D_Rectangle->calCostOfLand(pShape2D_Rectangle);
	//--------------------------------------------------------------------------------//
	double areaCircle          = pShape2D_Circle->getArea();
	double costOfLandCircle    = pShape2D_Circle->calCostOfLand(pShape2D_Circle);
	//--------------------------------------------------------------------------------//
	double areaTriangle        = pShape2D_Triangle->getArea();
	double costOfLandTriangle  = pShape2D_Triangle->calCostOfLand(pShape2D_Triangle);
	//--------------------------------------------------------------------------------//

	// Return memory of using
	delete pShape2D_Rectangle;
	delete pShape2D_Circle;
	delete pShape2D_Triangle;

	cout << "**************************************"		<< endl;
	cout << "*            Area & Cost             *"		<< endl;
	cout << "**************************************"		<< endl;
	cout << "Rectangle : Width        = 56.98"				<< endl;
	cout << "            Height       = 9.423"				<< endl;
	cout << "          : Area         = "					<< areaRectangle		<< endl;
	cout << "          : Cost of Land = "					<< costOfLandRectangle	<< endl;
	cout << "-------------------------------------------"	<< endl;
	cout << "Circle    : Redius       = 7.5891"				<< endl;
	cout << "          : Area         = "					<< areaCircle			<< endl;
	cout << "          : Cost of Land = "					<< costOfLandCircle		<< endl;
	cout << "-------------------------------------------"	<< endl;
	cout << "Triangle  : Base         = 658.394"			<< endl;
	cout << "          : Height       = 80.2255"			<< endl;
	cout << "          : Area         = "					<< areaTriangle			<< endl;
	cout << "          : Cost of Land = "					<< costOfLandTriangle	<< endl;
	cout << "*******************************************"	<< endl;
}